#include <eigen3/Eigen/Eigenvalues>

#include "linear_discriminant.hpp"

LinearDiscriminant::LinearDiscriminant(unsigned dimension)
    : dimension(dimension)
{
    matrix_[unsigned(Matrix::MixedMean)     ] = Eigen::VectorXd::Zero(dimension);
    matrix_[unsigned(Matrix::MixedScatter)  ] = Eigen::MatrixXd::Zero(dimension, dimension);
    matrix_[unsigned(Matrix::BetweenScatter)] = Eigen::MatrixXd::Zero(dimension, dimension);
    matrix_[unsigned(Matrix::WithinScatter) ] = Eigen::MatrixXd::Zero(dimension, dimension);
}

void LinearDiscriminant::add(feature_t* f, int class_id)
{
    ++num_items_;
    stats_computed_ = false;

    class_map_[class_id].push_back(f);
}

void LinearDiscriminant::compute_stats()
{
    if (stats_computed_) return;

    for (Eigen::MatrixXd & m : matrix_) m.setZero();

    Eigen::VectorXd total_sum  = Eigen::VectorXd::Zero(dimension);
    Eigen::MatrixXd total_sscp = Eigen::MatrixXd::Zero(dimension, dimension);

    Eigen::VectorXd class_sum  = Eigen::VectorXd::Zero(dimension);
    Eigen::MatrixXd class_sscp = Eigen::MatrixXd::Zero(dimension, dimension);

    for (std::pair<int, std::vector<feature_t*>> p : class_map_)
    {
        class_sum.setZero();
        class_sscp.setZero();

        for (feature_t* f : p.second)
        {
            class_sum.noalias()  += *f;
            class_sscp.noalias() += outer_prod(*f);
        }

        unsigned n = p.second.size();

        total_sum.noalias()  += class_sum;
        total_sscp.noalias() += class_sscp;

        Eigen::VectorXd class_mean    = class_sum/n;
        Eigen::MatrixXd class_scatter = class_sscp/n - outer_prod(class_mean);

        class_stats_.insert(std::make_pair(p.first, ClassStats(n, class_mean, class_scatter)));

        matrix_[unsigned( Matrix::WithinScatter )].noalias() += class_scatter;
    }

    matrix_[unsigned( Matrix::WithinScatter )] /= class_map_.size();
    matrix_[unsigned( Matrix::MixedMean )]      = total_sum/num_items_;
    matrix_[unsigned( Matrix::MixedScatter )]   = total_sscp/num_items_ - outer_prod(matrix_[unsigned( Matrix::MixedMean )]);


    matrix_[unsigned( Matrix::BetweenScatter )].setZero();
    for (std::pair<int, ClassStats> p : class_stats_)
        matrix_[unsigned( Matrix::BetweenScatter )].noalias() += outer_prod(p.second.mean - matrix_[unsigned( Matrix::MixedMean )]);

    matrix_[unsigned( Matrix::BetweenScatter )] /= class_stats_.size();

    stats_computed_ = true;
}


#include <iostream>
void LinearDiscriminant::evaluate(unsigned out_dimension)
{
    compute_stats();

    proj_mat_ = Eigen::MatrixXd::Zero(dimension, out_dimension);

    Eigen::MatrixXd nominator   = matrix_[unsigned( options.nominator )];
    Eigen::MatrixXd denominator = matrix_[unsigned( options.denominator )];

    if (options.qr_preprocess)
    {}

    if (options.diag_enhance)
    {}

    switch (options.criterion)
    {
        case Criterion::RatioOfTrace:
        {

            break;
        }
        case Criterion::TraceOfRatio:
        {
            Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXd> solver;
            solver.compute(nominator, denominator);
            Eigen::MatrixXd eigvecs = solver.eigenvectors();
            proj_mat_ = eigvecs.block(0, dimension-out_dimension, dimension, out_dimension);

            break;
        }
    }
}
