#ifndef K_MEANS_HPP
#define K_MEANS_HPP

#include "hard_classifier.hpp"

namespace clust
{
    //! @class KMeans
    //! @brief k-means classifier as described in 
    //!        "Making k-means even faster", by Greg Hamerly (2010).
    class KMeans : public HardClassifier
    {
    public:
        
        KMeans(unsigned num_clusters);
        void bang();
        void initialize_centers(std::vector<point_t> & init_centers);
        
        double silhouette_width();
    private:
        
        struct KMCluster
        {
            KMCluster(Cluster & base);

            Cluster & base;
            Eigen::VectorXd member_sum;
            double distance_moved;
            double distance_to_closest;
        };

        struct KMDatum
        {
            KMDatum(Datum &  base);
                 
            Datum & base;
            KMCluster * cluster;
            double lower_bound;
            double upper_bound;
        };
        
        void initialize();
        void point_all_ctrs(KMDatum & d);
        void move_centers();
        void update_bounds();
        bool converged();
        
        std::vector<KMCluster> km_clusters_;
        std::vector<KMDatum>   km_data_;
    };
}

#endif  // K_MEANS_HPP
