#include "unsorted.hpp"

namespace clust
{
    
    std::vector<point_t> initial_centers(unsigned num_centers, Dataset * data)
    {
        std::vector<point_t> output;
        
        std::random_device rd;
        std::mt19937 gen( rd() );
        std::uniform_real_distribution<> dis(0.0, data->size());

        unsigned first_center = static_cast<unsigned>( dis(gen) );
        output.push_back( data->at(first_center).point );

        std::vector<double> cumsum( data->size() );
        
        while(output.size() < num_centers)
        {
            for (Datum & d : *data)
            {
                point_t closest = *std::min_element (output.begin(), output.end(),
                    [&](point_t c1, point_t c2)
                    { 
                        return (d.point - c1).squaredNorm() < 
                               (d.point - c2).squaredNorm(); 
                    });
                

                cumsum[d.id] = (d.point - closest).squaredNorm();
                
                if (d.id > 0)
                    cumsum[d.id] += cumsum[d.id-1];
            }
            
            std::uniform_real_distribution<> urd(0.0, cumsum.back());
            double sample = urd( gen );

            unsigned next_center = 
                std::distance(cumsum.begin(), 
                              std::find_if(cumsum.begin(), cumsum.end(),
                              [sample](double x) { return x > sample; }));
            
            output.push_back( data->at(next_center).point );
        }
        
        return output;
    }
    
}
