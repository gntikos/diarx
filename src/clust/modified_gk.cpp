#include <stdexcept>

#include "modified_gk.hpp"
#include "unsorted.hpp"

namespace clust
{
    
ModifiedGK::ModifiedGK(unsigned dimension, unsigned num_clusters)
    : FuzzyClassifier( dimension )
    , beta           ( 1e5 )
    , gamma          ( 0.7 )
    , scaled_id_     ( Eigen::MatrixXd::Identity(dimension, dimension) )
{
    num_clusters_ = num_clusters;
}

void ModifiedGK::bang()
{
    if ( !data_ )
        throw std::runtime_error("[ModifiedGK]: No data set.");
    
    if (clusters_.size() == 0)
        initialize();
    
    while ( !converged_ )
    {
        update_prototypes();
        update_covariances();
        update_distances();
        update_partition();
    }
    
    finalize();
}

void ModifiedGK::initialize()
{
    // create empty clusters
    for (unsigned i=0; i<num_clusters_; ++i)
        clusters_.emplace_back(i, dimension_);
    
    // set matrix dimensions
    partition_matrix_ = Eigen::MatrixXd(data_->size(), clusters_.size());
    distance_matrix_  = Eigen::MatrixXd(data_->size(), clusters_.size());

    // compute the scaled identity matrix
    Eigen::MatrixXd cov = Eigen::MatrixXd::Zero(dimension_, dimension_);
    Eigen::VectorXd sum = Eigen::VectorXd::Zero(dimension_);
    
    double n = static_cast<double>( data_->size() );
    
    for (Datum & d : *data_)
    {
        sum.noalias() += d.point;
        cov.noalias() += d.point * d.point.transpose();
    }
    
    cov = cov/(n - 1.0) - sum*sum.transpose()/(n*n - n);
    
    double scale_factor = pow(cov.determinant(), 1.0/dimension_);
    scaled_id_.array() *= scale_factor;
    
    // initialize prototypes
    std::vector<point_t> init_centers = initial_centers(num_clusters_, data_);
    //get_init_centers(num_clusters_, data_, init_centers);
    
    for (unsigned i=0; i<num_clusters_; ++i)
        clusters_[i].mean = init_centers[i];
    
    // initialize distance matrix
    for (Cluster & c : clusters_)
        for (Datum & d : *data_)
            distance_matrix_(d.id, c.id) = (d.point - c.mean).squaredNorm() + 1e-100;
    distance_matrix_ = distance_matrix_.array().pow( -1.0 / (fuzz_exponent - 1.0) );
    
    // initialize partition matrix
    for (Datum & d : *data_)
        partition_matrix_.row(d.id).noalias() = 
            distance_matrix_.row(d.id) / distance_matrix_.row(d.id).sum();
    partition_matrix_ = partition_matrix_.array().pow(fuzz_exponent);
}

void ModifiedGK::update_prototypes()
{
    for (Cluster & c : clusters_)
    {
        c.mean.setZero();
        
        for (Datum & d : *data_)
        {
            c.mean.noalias() += partition_matrix_(d.id, c.id) * d.point;
        }
        
        c.mean.array() /= partition_matrix_.col(c.id).sum();
    }
}

void ModifiedGK::update_covariances()
{
    for (Cluster & c : clusters_)
    {
        c.sigma.setZero();
        
        for (Datum & d : *data_)
        {
            c.sigma.noalias() += partition_matrix_(d.id, c.id)
                * (d.point - c.mean) * (d.point - c.mean).transpose();
        }
        
        c.sigma.array() /= partition_matrix_.col(c.id).sum();
        
        c.sigma = (1.0 - gamma)*c.sigma + gamma*scaled_id_;
        
        eigsolver_.compute( c.sigma );
        
        double maxval = eigsolver_.eigenvalues().maxCoeff();
        double minval = eigsolver_.eigenvalues().minCoeff();
        
        if (maxval/minval > beta)
        {
            Eigen::VectorXd lim_eigvals = 
                (eigsolver_.eigenvalues().array() < maxval/beta)
                .select(maxval/beta, eigsolver_.eigenvalues());
                
            c.sigma.noalias() = eigsolver_.eigenvectors()
                * lim_eigvals.asDiagonal()
                * eigsolver_.eigenvectors().inverse();
        }
    }
}

void ModifiedGK::update_distances()
{
    for (Cluster & c : clusters_)
    {
        auto alpha = c.sigma.inverse();
        alpha.array() *= c.volume * pow(c.sigma.determinant(), 1.0/dimension_);
        
        for (Datum & d : *data_)
        {
            distance_matrix_(d.id, c.id) = 
                (d.point - c.mean).transpose() * alpha * (d.point - c.mean)
                + 1e-100;
        }
    }
    
    distance_matrix_ = 
        distance_matrix_.array().pow(-1.0 / (fuzz_exponent - 1));
}

void ModifiedGK::update_partition()
{
    double norm_diff = 0.0;
    Eigen::VectorXd row_pre, row_post;
    
    for (unsigned i=0; i<partition_matrix_.rows(); ++i)
    {
        row_pre  = partition_matrix_.row(i).array().pow(1.0 / fuzz_exponent);
        row_post = distance_matrix_.row(i) / distance_matrix_.row(i).sum();
        
        double diff = (row_pre - row_post).array().abs().maxCoeff();
        norm_diff   = std::max( norm_diff, diff );
        
        partition_matrix_.row(i) = row_post;
    }
    
    converged_ = norm_diff < threshold;
    if ( !converged_ )
        partition_matrix_ = partition_matrix_.array().pow(fuzz_exponent);
}


void ModifiedGK::finalize()
{
    Eigen::MatrixXd partition_m = partition_matrix_.array().pow(fuzz_exponent);
    
    for (Cluster & c : clusters_)
    {
        c.sigma.setZero();
        
        for (Datum & d : *data_)
        {
            c.sigma.noalias() += partition_m(d.id, c.id) * 
                (d.point - c.mean) * (d.point - c.mean).transpose();
        }
        
    }
}
    
    
}
