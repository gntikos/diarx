#ifndef HARD_CLASSIFIER_HPP
#define HARD_CLASSIFIER_HPP

#include <vector>
#include <memory>

#include <eigen3/Eigen/Core>

#include "unsorted.hpp"

namespace clust
{
    
    class HardClassifier
    {
    public:
        HardClassifier(unsigned num_clusters = 0)
            : threshold      ( 1e-8 )
            , max_iterations ( 3050 )
            , num_clusters_  ( num_clusters )
            , data_          ( nullptr )
        {}
        
        void set_data(Dataset* data)
            { data_ = data; }
            
        const Eigen::VectorXd & partition() const
            { return partition_; }
            
        unsigned partition(unsigned data_id) const
            { return partition_( data_id ); }
        
        virtual 
        void bang() = 0;
        
        virtual 
        void initialize_centers(std::vector<point_t> & init_centers) = 0;
        
        virtual ~HardClassifier() {}
        
        double threshold;
        unsigned max_iterations;
        
    protected:
        
        struct Cluster
        {
            Cluster(point_t & center, unsigned id)
                : center      ( center )
                , num_members ( 0 )
                , radius      ( 0.0 )
                , id          ( id )
            {}
            
            point_t  center;
            unsigned num_members;
            double   radius;
            const unsigned id;
        };
        
        unsigned num_clusters_;
        
        Dataset*   data_;
        std::vector<Cluster> clusters_;
        
        Eigen::VectorXd partition_;
    };

}

#endif  // HARD_CLASSIFIER_HPP
