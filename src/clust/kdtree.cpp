#include <vector>
#include <algorithm>

#include "kdtree.hpp"

namespace clust
{
    void KdTree::bang()
    {
        unsigned dimension = data_[0].point.rows();
        
        std::vector<unsigned> indices( data_.size() );
        for (unsigned i=0; i<indices.size(); ++i)
            indices[i] = i;
        
        auto middle = indices.begin() + 
                      std::distance(indices.begin(), indices.end()) / 2;
        
        std::partial_sort(indices.begin(), indices.end(), middle,
                [=](unsigned i, unsigned j)
                {
                    return data_[i].point(0) < data_[j].point(0);
                });
        
        middle = indices.begin() + 
                      std::distance(indices.begin(), indices.end()) / 2;
        
        root_.reset(new KdNode(*middle));
        
        unsigned next = 1 % data_[0].point.rows();
        
        range_t left_range = std::make_pair(indices.begin(), middle);
        if ( std::distance(left_range.first, left_range.second) >= 2 )
            root_->left.reset  (new KdNode(root_.get(), &data_, indices, left_range, next));
        
        range_t right_range = std::make_pair(middle+1, indices.end());
        if ( std::distance(right_range.first, right_range.second) >= 2)
            root_->right.reset (new KdNode(root_.get(), &data_, indices, right_range,   next));
    }

    
    KdTree::KdNode::KdNode(unsigned int node_id)
        : id    ( node_id )
        , parent( nullptr )
        , left  ( nullptr )
        , right ( nullptr )
        {}
    
    KdTree::KdNode::KdNode(const KdNode * parent, 
                           const Dataset * data, 
                           KdTree::IndicesList & indices, 
                           range_t range, 
                           unsigned int axis)
        : parent(parent)
        , left ( nullptr )
        , right( nullptr )
    {
        auto middle = range.first + std::distance(range.first, range.second)/2;
        
        std::partial_sort(range.first, range.second, middle,
                [=](unsigned i, unsigned j)
                {
                    return data->at(i).point(axis) < data->at(j).point(axis);
                });
        
        unsigned next = (axis + 1) % data->at(0).point.rows();
        
        id = *middle;
        
        range_t left_range = std::make_pair(range.first, middle);
        if ( std::distance(left_range.first, left_range.second) >= 2 )
           left.reset (new KdNode(this, data, indices, left_range, next));
        
        range_t right_range = std::make_pair(middle+1, range.second);
        if ( std::distance(right_range.first, right_range.second) >= 2)
           right.reset (new KdNode(this, data, indices, right_range, next));
    }

}