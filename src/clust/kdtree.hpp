#ifndef KD_TREE_HPP
#define KD_TREE_HPP

#include <vector>
#include <memory>
#include <utility>
#include <iostream>

#include "unsorted.hpp"

namespace clust
{
    class KdTree
    {    
    public:
        
        class KdNode; // forward declaration

        typedef std::vector<unsigned> IndicesList;
        typedef std::pair<IndicesList::iterator, IndicesList::iterator> range_t;
        typedef std::shared_ptr<KdNode> node_t;
        
        class KdNode
        {
            friend class KdTree;
    
        public:            
            const KdNode * parent; //!< the parent KdNode
            node_t left;           //!< the left child KdNode
            node_t right;          //!< the right child KdNode
            
            unsigned id;           //!< the index of the KdNode element in KdTree::data_
            
        private:
            
            //! Constructor
            //! 
            //! @brief This constructor is called from a KdNode
            //!        to generate its children.
            //!
            //! @param parent  the parent KdNode
            //! @param data    the KdTree's Dataset
            //! @param indices the indices of the \sa data elements
            //! @param range   the range of elements in \sa indices
            //!                that belong to this part of the tree.
            //! @param axis    the axis where the split will be done.
            KdNode(const KdNode * parent,
                   const Dataset * data, 
                   IndicesList & indices, range_t range, 
                   unsigned axis);
            
            //! Constructor
            //!
            //! @brief This constructor is called by the KdTree
            //!        itsself to create the root KdNode.
            //! 
            //! @param node_id the index in KdTree::data_ of the element
            //!                that will be the root node.
            KdNode(unsigned node_id);
                
        };
        
        //! Constructor
        //!
        //! @param data the Dataset where the tree is built.
        KdTree (const Dataset & data)
            : data_( data )
        {}
        
        void bang();
        
        node_t const & root() const
            { return root_; }
        
        void cut_node(std::shared_ptr<KdNode> & branch)
        {
            if (branch->left)
                cut_node(branch->left);
            branch->left.reset();
            
            if (branch->right)
                cut_node(branch->right);
            branch->right.reset();
            
            branch.reset();
        }
        
    private:
        node_t root_;
        const Dataset & data_;
    };
}

#endif  // KD_TREE_HPP
