#ifndef FUZZY_CLASSIFIER_HPP
#define FUZZY_CLASSIFIER_HPP

#include <vector>
#include <boost/concept_check.hpp>

#include <eigen3/Eigen/Core>

#include "unsorted.hpp"

namespace clust
{
    class FuzzyClassifier
    {
    public:
        virtual void bang() = 0;
        virtual ~FuzzyClassifier() {}
        
        struct Cluster
        {
        public:
            Cluster(unsigned id, double volume = 1.0)
                : id     ( id )
                , volume ( volume )
            {}
            
            Cluster(unsigned id, unsigned dim, double volume = 1.0)
                : id          ( id )
                , volume      ( volume )
                , mean        ( Eigen::VectorXd::Zero(dim) )
                , sigma       ( Eigen::MatrixXd::Zero(dim, dim) )
                , norm_matrix ( Eigen::MatrixXd::Identity(dim, dim) )
            {}
            
            const Eigen::VectorXd & Mean() const
                { return mean; }
                
            const Eigen::MatrixXd & Sigma() const
                { return sigma; }
            
            const unsigned id;
    
        //protected:
            
            double volume;
            
            Eigen::VectorXd mean;
            Eigen::MatrixXd sigma;
            Eigen::MatrixXd norm_matrix;
        };
        
        void set_data(Dataset* data)
            { data_ = data; }
        
        const Eigen::MatrixXd & partition() const
            { return partition_matrix_; }
            
        const double partition(unsigned sample, unsigned cluster) const
            { return partition_matrix_(sample, cluster); }
            
        const std::vector<Cluster> & clusters() const
            { return clusters_; }
           
        double   fuzz_exponent;
        double   threshold;
        unsigned max_iterations;
        
        enum class ValidityMeasure
        {
            XieBeni,
            FukuyamaSugeno
        };

        double validity_measure(ValidityMeasure type)
        {
            double index = 0.0;

            switch (type)
            {
                case FuzzyClassifier::ValidityMeasure::XieBeni:
                {
                    for (Datum & d : *data_)
                    {
                        for (Cluster & c : clusters_)
                        {
                            index += pow(partition_matrix_(d.id, c.id), fuzz_exponent) *
                                     (d.point - c.mean).norm();

                        }
                    }

                    double min_dist = std::numeric_limits<double>::max();
                    for (unsigned i=0; i<clusters_.size(); ++i)
                        for (unsigned j=0; j<i; ++j)
                            min_dist = std::min( (clusters_[i].mean - clusters_[j].mean).squaredNorm() , min_dist );

                    index /= (data_->size() * min_dist);

                    break;
                }

                case FuzzyClassifier::ValidityMeasure::FukuyamaSugeno:
                {
                    break;
                }
            }

            return index;
        }

    protected:
        
        FuzzyClassifier(unsigned dimension)
            : fuzz_exponent  ( 2.0 )
            , threshold      ( 1e-5 )
            , max_iterations ( 20 )
            , data_          ( nullptr )
            , dimension_     ( dimension )
            , converged_     ( false )
        {}
        
        
        
        Eigen::MatrixXd partition_matrix_;
        
        Dataset*   data_;
        std::vector<Cluster> clusters_;
        
        const unsigned dimension_;
        unsigned       num_clusters_;
        bool           converged_;
    };
    
}

#endif

