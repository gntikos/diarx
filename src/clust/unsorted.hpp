#ifndef UNSORTED_HPP
#define UNSORTED_HPP

#include <vector>

#include <eigen3/Eigen/Core>

namespace clust
{
    struct Datum;
    typedef Eigen::VectorXd    point_t;
    typedef std::vector<Datum> Dataset;

    struct Datum 
    {
        Datum(const point_t & point, unsigned id)
            : point ( point )
            , id    ( id )
        {}
        
        const point_t & point;
        const unsigned id;
    };
    
    
    /// @fn    initial_centers
    /// @brief Finds the initial centers of k-means++ algorithm from a Dataset.
    ///
    /// @param num_centers The number of centers to find.
    /// @param data        The Dataset where the search will be done.
    std::vector<point_t> initial_centers(unsigned num_centers, Dataset * data);
                      
}

#endif
