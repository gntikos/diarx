#ifndef MODIFIED_GK_HPP
#define MODIFIED_GK_HPP

#include <eigen3/Eigen/Eigenvalues>

#include "fuzzy_classifier.hpp"

namespace clust
{
    
    class ModifiedGK : public FuzzyClassifier
    {
    public:
        ModifiedGK(unsigned dimension, unsigned num_clusters);
        ~ModifiedGK() {}
        
        void bang();
        void merge_clusters(unsigned c1_id, unsigned c2_id)
        {
            partition_matrix_.col(c1_id).noalias() += partition_matrix_.col(c2_id);
            partition_matrix_.col(c2_id).setOnes();
            partition_matrix_.col(c2_id).array() *= 1e-20;
            clusters_[c2_id].sigma.setZero();
            for (unsigned i=0; i<clusters_[c2_id].sigma.rows(); ++i)
                clusters_[c2_id].sigma(i, i) = 1e-20;
            clusters_[c1_id].mean = (clusters_[c1_id].mean + clusters_[c2_id].mean)/2.0;
            
            clusters_[c2_id].mean.setOnes();
            clusters_[c2_id].mean.array() *= std::numeric_limits<double>::max();
            converged_ = false;
            bang();
        }

        double beta;
        double gamma;

        
    private:
        void initialize();
        void update_prototypes();
        void update_covariances();
        void update_distances();
        void update_partition();
        void finalize();
        
        Eigen::MatrixXd scaled_id_;
        Eigen::MatrixXd distance_matrix_;
        
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigsolver_;
    };

}

#endif

