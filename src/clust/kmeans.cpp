#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <limits>
#include <iostream>

#include "kmeans.hpp"
#include "unsorted.hpp"

namespace clust
{

    KMeans::KMCluster::KMCluster(HardClassifier::Cluster & base)
        : base(base)
        , member_sum( (Eigen::VectorXd::Zero(base.center.rows())) )
        , distance_moved( std::numeric_limits<double>::max() )
        , distance_to_closest( 0.0 )
    {}

    KMeans::KMDatum::KMDatum(Datum & base)
        : base (base)
        , cluster (nullptr)
        , lower_bound(0.0)
        , upper_bound(0.0)
    {}

    KMeans::KMeans(unsigned num_clusters)
        : HardClassifier( num_clusters )
    {}

    void KMeans::initialize_centers(std::vector<point_t> & init_centers)
    {
        if ( init_centers.size() != num_clusters_ )
            throw std::runtime_error("Invalid number of initial centers");
        
        for ( point_t & p : init_centers )
            clusters_.emplace_back( p, clusters_.size() );
    }

    
    void KMeans::bang()
    {
        if ( !data_ )
            throw std::runtime_error("No data set.");
        
        initialize();        
        unsigned num_iterations = 0;
        
        while ( !converged() && num_iterations++ < max_iterations)
        {
            for ( KMDatum & d : km_data_ )
            {
                double m = std::max(d.lower_bound, d.cluster->distance_moved/2.0);
                
                if ( d.upper_bound > m )
                {
                    d.upper_bound = (d.base.point - d.cluster->base.center).norm();
                    
                    if ( d.upper_bound > m )
                    {
                        auto prev = d.cluster;
                        point_all_ctrs( d );
                        
                        if ( d.cluster != prev )
                        {
                            prev->base.num_members--;
                            prev->member_sum.noalias() -= d.base.point;
                            
                            d.cluster->base.num_members++;
                            d.cluster->member_sum.noalias() += d.base.point;
                        }
                    }
                }
            }
            
            move_centers();
            update_bounds();
        }
        
        partition_ = Eigen::VectorXd( km_data_.size() );
        for (KMDatum & d : km_data_)
            partition_(d.base.id) = d.cluster->base.id;
    }
    
    void KMeans::initialize()
    {
        for (Datum & d : *data_)
            km_data_.emplace_back(d);
        
        if ( clusters_.size() == 0 )
        {
            std::vector<point_t> init_centers = initial_centers(num_clusters_, data_);
            
            for ( point_t & p : init_centers )
            {
                clusters_.emplace_back(p, clusters_.size());
            }
        }
        
        for (Cluster & c : clusters_)
            km_clusters_.emplace_back(c);
        
        for ( KMDatum & d : km_data_ )
        {
            point_all_ctrs( d );
            d.cluster->base.num_members++;
            d.cluster->member_sum.noalias() += d.base.point;
        }
    }
    

    void KMeans::point_all_ctrs(KMDatum & d)
    {
        double   min_dist      = std::numeric_limits<double>::max();
        double   next_min_dist = std::numeric_limits<double>::max();
        unsigned min_index     = 0;
        
        for (Cluster & c : clusters_)
        {
            double dist = (d.base.point - c.center).squaredNorm();
            
            if (dist < next_min_dist)
            {
                if (dist < min_dist)
                {
                    next_min_dist = min_dist;
                    min_dist      = dist;
                    min_index     = c.id;
                }
                else
                {
                    next_min_dist = dist;
                }
            }
        }
        
        d.cluster     = &km_clusters_[min_index];
        d.lower_bound = sqrt(min_dist);
        d.upper_bound = sqrt(next_min_dist);
    }
    
    
    void KMeans::move_centers()
    {
        point_t prev_center;
        
        for ( KMCluster & c : km_clusters_ )
        {
            prev_center = c.base.center;
            c.base.center = c.member_sum / static_cast<double>(c.base.num_members);
            c.distance_moved = (c.base.center - prev_center).norm();
        }
    }
    
    void KMeans::update_bounds()
    {        
        std::vector<unsigned> ind(num_clusters_);
        for (unsigned i=0; i<num_clusters_; ++i)
            ind[i] = i;
        
        std::partial_sort(ind.begin(), ind.begin() + 2, ind.end(),
            [&](unsigned i, unsigned j)
            {
                return km_clusters_[i].distance_moved < km_clusters_[j].distance_moved;
            });
        
        for ( KMDatum & d : km_data_ )
        {
            d.upper_bound += d.cluster->distance_moved;
            
            if ( d.cluster->base.id == ind[0] )
                d.lower_bound -= km_clusters_[ ind[1] ].distance_moved;
            else
                d.lower_bound -= km_clusters_[ ind[0] ].distance_moved;
        }
    }


    bool KMeans::converged()
    {
        for ( KMCluster & c : km_clusters_ )
            if ( c.distance_moved > threshold )
                return false;
        return true;
    }
    
    double KMeans::silhouette_width()
    {
        double sw = 0.0;
        std::vector<double> dists(num_clusters_, 0.0);
        
        for (KMDatum & d : km_data_)
        {
            if (d.cluster->base.num_members <= 5)
                continue;
            
            for (unsigned i=0; i<num_clusters_; ++i)
                dists[i] = (d.base.point - km_clusters_[i].base.center).norm();
            
            std::sort(dists.begin(), dists.end());
            
            double a = dists[0];
            double b = dists[1];
            
            sw += (b-a)/std::max(a,b);
        }
        
        return sw / (data_->size());
    }

    
}