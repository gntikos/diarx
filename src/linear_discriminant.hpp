#ifndef LINEAR_DISCRIMINANT_HPP
#define LINEAR_DISCRIMINANT_HPP

#include <array>
#include <functional>
#include <map>
#include <vector>

#include <eigen3/Eigen/Core>

class LinearDiscriminant
{
    typedef Eigen::VectorXd feature_t;

public:

    enum class Matrix : unsigned
    {
        MixedMean      = 0,
        MixedScatter   = 1,
        WithinScatter  = 2,
        BetweenScatter = 3
    };

    enum class Criterion
    {
        TraceOfRatio = 1,
        RatioOfTrace = 2
    };

    struct ClassStats
    {
        ClassStats(unsigned n, Eigen::VectorXd & m, Eigen::MatrixXd & s)
            : num_items(n), mean(m), scatter(s)
        {}

        unsigned num_items;
        Eigen::VectorXd mean;
        Eigen::MatrixXd scatter;
    };

    struct Options
    {
        Matrix nominator    = Matrix::MixedScatter;
        Matrix denominator  = Matrix::WithinScatter;
        Criterion criterion = Criterion::TraceOfRatio;
        bool qr_preprocess = false;
        bool diag_enhance  = false;
    } options;


    LinearDiscriminant(unsigned dimension);
    void add(feature_t* f, int class_id);
    void evaluate(unsigned out_dimension);
    const Eigen::MatrixXd & projection_matrix() const { return proj_mat_; }

    const unsigned num_classes() const { return class_map_.size(); }
    const unsigned num_items() const { return num_items_; }

    const unsigned dimension;

//private:
    void compute_stats();

    inline Eigen::MatrixXd outer_prod(const Eigen::MatrixXd & m)
        { return m * m.transpose(); }

    std::map<int, std::vector<feature_t*>> class_map_;
    std::map<int, ClassStats> class_stats_;

    std::array<Eigen::MatrixXd, 4> matrix_;
    Eigen::MatrixXd proj_mat_;

    unsigned num_items_ = 0;
    bool stats_computed_ = false;
};

#endif // LINEAR_DISCRIMINANT_HPP
