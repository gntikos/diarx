#ifndef TRANSCRIPTION_HPP
#define TRANSCRIPTION_HPP

#include <string>
#include <vector>
#include <set>

#include <boost/property_tree/ptree.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

class Transcription
{
public:
    struct Speaker
    {
        std::string id;   // speaker's identifier in the transcription
        std::string name = "Unknown Speaker"; // speaker's actual name

        enum class Gender { Unknown = 0, Male, Female };
        enum class Dialect { Native = 0, NonNative };

        Gender gender = Gender::Unknown;
        Dialect dialect = Dialect::Native;

        void write_xml_repr(boost::property_tree::ptree & parent_node);
    };

    struct Turn
    {
        Turn(unsigned start, unsigned end, std::set<unsigned> & speakers)
            : start(start)
            , end(end)
            , speakers(speakers)
        {}

        unsigned start;
        unsigned end;
        std::set<unsigned> speakers;

        void write_xml_repr(boost::property_tree::ptree & parent_node, Transcription * parent);
    };

    struct Info
    {
        std::string audio_filename = "";
        std::string transcriber = "diarx";
        std::string version = "0.1";
        std::string version_date = "";
        std::string language = "";
        std::string program = "";
        std::string air_data = "";
    } info;

    float time_unit = -1.0; // how much time is contained per frame

    void set_num_speakers(unsigned num_speakers);
    void step_frame(std::set<unsigned> &speakers);
    void write_xml_repr(std::string trs_filename);

    Speaker & speaker(unsigned id);

private:
    std::vector<Speaker> speakers_;
    std::vector<Turn> turns_;
};

#endif // TRANSCRIPTION_HPP
