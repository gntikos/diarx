#ifndef PREEMPHASIS_FILTER_HPP
#define PREEMPHASIS_FILTER_HPP

#include "processor.hpp"
#include "short_term_feature.hpp"

class PreemphasisFilter : public Processor<Sequence, Sequence>
{
public:
    PreemphasisFilter(settings_t & settings);
    bool bang(const Sequence & input);

    const double coeff;
};

#endif // PREEMPHASIS_FILTER_HPP
