#include <fstream>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "diarx.hpp"
#include "clust/modified_gk.hpp"
#include "smoother.hpp"
#include "transcription.hpp"

namespace po = boost::program_options;

int main(int argc, char** argv)
{
    // 1. Parse command line arguments
    // ------------------------------------------------------------------------
    std::string audio_filename, config_filename, transcription_filename, output_filename;

    po::options_description opt_desc("diarX options");
    opt_desc.add_options()
            ("help,h", "this help message")
            ("input,i", po::value<std::string>(&audio_filename)->required(),
                "input audio file")
            ("config,c", po::value<std::string>(&config_filename)->default_value("../settings.json"),
                "configuration JSON file")
            ("transcription,t", po::value<std::string>(&transcription_filename)->default_value(""),
                "transcription file")
            ("output,o", po::value<std::string>(&output_filename)->default_value(""),
                "output file")
            ;
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opt_desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return -1;
    }

    po::notify(vm);

    settings_t settings;
    boost::property_tree::json_parser::read_json(config_filename, settings);


    // 2. Open audio file and create the audio processing chain
    // ------------------------------------------------------------------------
    std::cerr << "Opening audio file... ";
    AudioPimp audio(audio_filename, settings);
    std::cout << "(duration: " << audio.duration << " sec.) ";
    settings = reformat_settings(settings, audio.samplerate);
    std::cerr << "done." << std::endl;

    PreemphasisFilter pf(settings);
    Window win(settings);
    STFT stft(settings);
    MelFilterbank mfb(settings);
    MFCC mfcc(settings);

    TextureWindow tw(settings.get<unsigned>("flsd.texture_window_size"),
                     settings.get<unsigned>("flsd.texture_window_step"),
                     mfcc.num_coefs);

    // 3. Extract features
    // -------------------------------------------------------------------------
    std::vector<RealVector> features;

    std::cerr << "Getting features... ";
    while (audio.bang())
    {
        pf.bang(audio.output);
        win.bang(pf.output);
        stft.bang(win.output);
        mfb.bang(stft.output);
        mfcc.bang(mfb.output);

        if (tw.bang(mfcc.output))
            features.push_back(tw.output);
    }
    std::cerr << " done." << std::endl;


    // 4. Project to FLsD subspace
    // ------------------------------------------------------------------------
    std::cerr << "Computing projection matrix... ";
    LinearDiscriminant ld(tw.dimension);

    unsigned wins_per_thread = settings.get<unsigned>("flsd.speaker_thread_duration");
    unsigned num_left_for_thread = wins_per_thread;
    unsigned class_id = 0;

    for (RealVector & f : features)
    {
        ld.add(&f.value, class_id);
        --num_left_for_thread;

        if (num_left_for_thread == 0)
        {
            ++class_id;
            num_left_for_thread = wins_per_thread;
        }
    }


    ld.evaluate(settings.get<unsigned>("flsd.subspace_dimension"));
    std::cout << "(subspace dimension: " << ld.projection_matrix().cols() << ") ";
    std::cout << " done." << std::endl;


    // 5. Cluster features
    // ------------------------------------------------------------------------
    std::cout << "Clustering data... ";
    std::vector<Eigen::VectorXd> projected_features;

    if (settings.get<bool>("threads_as_data"))
    {
        for (auto p : ld.class_stats_)
            projected_features.push_back(ld.projection_matrix().transpose() * p.second.mean * wins_per_thread);
    }
    else
    {
        for (std::pair<int, std::vector<Eigen::VectorXd*>> p : ld.class_map_)
            for (Eigen::VectorXd* f : p.second)
                projected_features.push_back(ld.projection_matrix().transpose() * (*f));
    }

    clust::Dataset dataset;
    for (Eigen::VectorXd & pf : projected_features)
        dataset.emplace_back(pf, dataset.size());

    std::cout << "(# data: " << dataset.size();

    clust::ModifiedGK mgk(ld.projection_matrix().cols(), settings.get<unsigned>("number_of_speakers"));
    mgk.set_data(&dataset);
    mgk.bang();
    std::cerr << ", number of speakers: " << mgk.clusters().size() << ") ";
    std::cerr << "done." << std::endl;

    Smoother s(mgk.partition());
    s.bang();

    if (!output_filename.empty())
    {
        std::cout << "Writing results to " << output_filename << "... ";
        std::ofstream out(output_filename);

        out << "# the first " << ld.projection_matrix().cols() << " columns is data.\n";
        out << "# the next " << s.partition().cols() <<
               " columns contain the presence probability for each speaker\n";
        out << std::endl;

        for (unsigned i=0; i<s.partition().rows(); ++i)
            out << dataset[i].point.transpose() << " " << s.partition().row(i) << std::endl;

        out.close();
        std::cout << "done." << std::endl;
    }


    if (!transcription_filename.empty())
    {
        std::cout << "Writing transcription to " << transcription_filename << "... ";

        Transcription trs;
        trs.info.audio_filename = audio_filename;
        trs.set_num_speakers(settings.get<unsigned>("number_of_speakers"));
        trs.time_unit = audio.duration * 1000.0 / s.partition().rows(); // kinda wrong, but still...
        for (unsigned i=0; i<s.partition().rows(); ++i)
        {
            unsigned sp;
            s.partition().row(i).maxCoeff(&sp);
            std::set<unsigned> sss {sp};
            trs.step_frame(sss);
        }

        trs.write_xml_repr(transcription_filename);
        std::cout << "done." << std::endl;
    }

    std::cerr << "Clustering validity measure (Xie-Beni): ";
    std::cerr << mgk.validity_measure(clust::FuzzyClassifier::ValidityMeasure::XieBeni) << std::endl;
    std::cerr << "That's all." << std::endl;

    return 0;
}

