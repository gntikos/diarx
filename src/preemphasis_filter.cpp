#include "preemphasis_filter.hpp"

PreemphasisFilter::PreemphasisFilter(settings_t & settings)
    : coeff(settings.get<double>("preemphasis_coeff"))
{
    output.value.resize(settings.get<unsigned>("frame_size"));
}

bool PreemphasisFilter::bang(const Sequence & input)
{
    output.value[0] = input.value[0];
    for (unsigned i=1; i<input.value.size(); ++i)
        output.value[i] = input.value[i] - coeff*input.value[i-1];

    output.from = input.from;
    output.to   = input.to;

    return true;
}
