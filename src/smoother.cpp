#include <iostream>
#include "smoother.hpp"
#include <eigen3/Eigen/LU>


Smoother::Smoother(Eigen::MatrixXd const & partition)
    : num_states( partition.cols() )
    , num_observations( partition.rows() )
    , transition_matrix_( Eigen::MatrixXd::Zero(num_states, num_states) )
    , partition_( partition )
    , state_seq_(num_observations)
{
    for (unsigned i=0; i<partition.rows(); ++i)
        partition.row(i).maxCoeff( &state_seq_[i] );

    for (unsigned i=1; i<state_seq_.size(); ++i)
        transition_matrix_(state_seq_[i-1], state_seq_[i]) += 1.0;

    for (unsigned i=0; i<num_states; ++i)
        transition_matrix_.row(i) /= transition_matrix_.row(i).sum();
}

void Smoother::bang()
{
    unsigned T = num_observations;

    Eigen::VectorXd gamma_t (num_states);
    Eigen::VectorXd gamma_t1 = Eigen::VectorXd::Constant(num_states, 1.0/num_states);

    for (unsigned t=1; t<T;  ++t)
    {
        gamma_t = gamma_t1.cwiseProduct(partition_.row(t-1).transpose()); //ok
        gamma_t /= gamma_t.sum(); // ok
        gamma_t1 = gamma_t.transpose() * transition_matrix_;
       partition_.row(t-1) = gamma_t.transpose();
    }

    gamma_t = gamma_t1.cwiseProduct(partition_.row(T-1).transpose());
    gamma_t /= gamma_t.sum();
    partition_.row(T-1) = gamma_t;

    // fuck backward smooting!
}


