#include "texture_window.hpp"

TextureWindow::TextureWindow(unsigned win_len, unsigned win_step, unsigned in_dim)
    : win_length   (win_len)
    , win_step     (win_step)
    , in_dimension (in_dim)
    , dimension    (2*in_dim)
    , buffer_      (win_len)
    , sum_       (Eigen::VectorXd::Zero(in_dimension))
    , square_sum_(Eigen::VectorXd::Zero(in_dimension))
{
    output.value = Eigen::VectorXd::Zero(dimension);
}

bool TextureWindow::bang(const RealVector & input)
{
    static int items_to_next = win_length;

    buffer_.push_back(input);
    sum_.noalias() += input.value;
    square_sum_.noalias() += input.value.array().square().matrix();

    --items_to_next;

    if (items_to_next == 0)
    {
        for (unsigned i=0; i<in_dimension; ++i)
        {
            output.value(i) = sum_(i) / win_length;
            output.value(in_dimension+i) = square_sum_(i)/win_length - output.value(i)*output.value(i);
        }

        for (unsigned i=0; i<win_step; ++i)
        {
            sum_.noalias() -= buffer_[i].value;
            square_sum_.noalias() -= buffer_[i].value.array().square().matrix();
        }

        items_to_next = win_step;

        output.from = buffer_[0].from;
        output.to   = buffer_.back().to;

        return true;
    }

    return false;
}
