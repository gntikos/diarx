#ifndef SMOOTHER_HPP
#define SMOOTHER_HPP

#include <vector>
#include <eigen3/Eigen/Core>

typedef unsigned state_t;

class Smoother
{
public:
    Smoother(Eigen::MatrixXd const & partition);

    const unsigned num_states;
    const unsigned num_observations;

    void bang();
    Eigen::MatrixXd & partition() { return partition_; }

private:
    Eigen::MatrixXd transition_matrix_;
    Eigen::MatrixXd partition_;
    std::vector<state_t> state_seq_;
};

#endif // SMOOTHER_HPP
