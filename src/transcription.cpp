#include <iostream>
#include <memory>
#include <sstream>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree_serialization.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include "transcription.hpp"

void Transcription::set_num_speakers(unsigned num_speakers)
{
    speakers_.resize(num_speakers);
    for (unsigned i=0; i<num_speakers; ++i)
    {
        speakers_[i].id = "spk" + boost::lexical_cast<std::string>(i);
        speakers_[i].name = "Unknown Speaker #" + boost::lexical_cast<std::string>(i);
    }
}

Transcription::Speaker & Transcription::speaker(unsigned id)
{
    return speakers_.at(id);
}

void Transcription::step_frame(std::set<unsigned> & speakers)
{
    if ( turns_.empty() )
        turns_.emplace_back(0, 1, speakers);
    else if ( turns_.back().speakers == speakers)
        ++turns_.back().end;
    else
        turns_.emplace_back( turns_.back().end, turns_.back().end+1, speakers );
}

void Transcription::write_xml_repr(std::string trs_filename)
{
    if ( time_unit == 0 )
    {
        std::cerr << "[Transcription]: time unit is not set." << std::endl;
        return;
    }

    std::ostringstream ss;
    ss.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%d %b %Y")));
    ss << boost::posix_time::second_clock::local_time();
    info.version_date = ss.str();

    boost::property_tree::ptree trs;
    trs.put("Trans.<xmlattr>.audio_filename", info.audio_filename);
    trs.put("Trans.<xmlattr>.scribe", info.transcriber);
    trs.put("Trans.<xmlattr>.version", info.version);
    trs.put("Trans.<xmlattr>.version_date", info.version_date);
    trs.put("Trans.<xmlattr>.xml:lang", info.language);

    for(Speaker & s : speakers_)
        s.write_xml_repr(trs.put("Trans.Speakers", ""));

    trs.put("Trans.Episode.<xmlattr>.program", info.program);

    for (Turn & t : turns_)
        t.write_xml_repr( trs.put("Trans.Episode.Section", ""), this);

    float trs_start = turns_[0].start * time_unit / 1000.0;
    float trs_end = turns_.back().end * time_unit / 1000.0;
    trs.put("Trans.Episode.Section.<xmlattr>.startTime", boost::format("%|0.3f|") % trs_start);
    trs.put("Trans.Episode.Section.<xmlattr>.endTime",   boost::format("%|0.3f|") % trs_end);



    boost::property_tree::xml_writer_settings<char> settings('\t', 1);
    boost::property_tree::xml_parser::write_xml(trs_filename, trs, std::locale(), settings);
}

void Transcription::Speaker::write_xml_repr(boost::property_tree::ptree &parent_node)
{
    boost::property_tree::ptree node;
    node.put("<xmlattr>.id", id);
    node.put("<xmlattr>.name", name);

    switch(gender)
    {
        case(Gender::Male):
            node.put("<xmlattr>.type", "male");
            break;
        case(Gender::Female):
            node.put("<xmlattr>.type", "female");
            break;
        case(Gender::Unknown):
            node.put("<xmlattr>.type", "unknown");
            break;
        default:
            node.put("<xmlattr>.type", "unknown");
            break;
    }

    parent_node.add_child("Speaker", node);
}

void Transcription::Turn::write_xml_repr(boost::property_tree::ptree &parent_node, Transcription *parent)
{
    boost::property_tree::ptree node;

    float start_time = start * parent->time_unit/1000.0;
    float end_time   = end   * parent->time_unit/1000.0;

    node.put("<xmlattr>.startTime", boost::format("%|0.3f|") % start_time);
    node.put("<xmlattr>.endTime",   boost::format("%|0.3f|") % end_time);
    node.put("Sync.<xmlattr>.time", boost::format("%|0.3f|") % start_time);

    std::string speakers_str = "";
    for (const unsigned & s : speakers)
        speakers_str += parent->speaker(s).id + " ";

    node.put("<xmlattr>.speaker", speakers_str);

    if (speakers.size() > 1)
    {
        for (unsigned i=1; i<=speakers.size(); ++i)
        {
            boost::property_tree::ptree spk_node;
            spk_node.put("<xmlattr>.nb", i);
            node.add_child("Who", spk_node);
        }
    }

    parent_node.add_child("Turn", node);
}


