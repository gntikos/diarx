#ifndef SHORT_TERM_FEATURE_HPP
#define SHORT_TERM_FEATURE_HPP

#include <vector>
#include <eigen3/Eigen/Core>


typedef unsigned            timepoint_t;

template<class FeatureType>
class ShortTermFeature
{
public:
    FeatureType value;
    timepoint_t from;
    timepoint_t to;
};

typedef std::vector<double> sequence_vec_t;
typedef Eigen::VectorXd     numeric_vec_t;
typedef ShortTermFeature<sequence_vec_t> Sequence;
typedef ShortTermFeature<numeric_vec_t>  RealVector;

#endif // SHORT_TERM_FEATURE_HPP
