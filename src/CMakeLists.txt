find_package(Boost COMPONENTS program_options REQUIRED)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})

file(GLOB diarx_srcs *.cpp)
add_executable(${PROJECT_NAME} ${diarx_srcs})
target_link_libraries(${PROJECT_NAME} clust fftw3 sndfile m ${Boost_LIBRARIES})