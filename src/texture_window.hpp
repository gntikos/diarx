#ifndef TEXTURE_WINDOW_HPP
#define TEXTURE_WINDOW_HPP

#include <boost/circular_buffer.hpp>

#include "processor.hpp"
#include "short_term_feature.hpp"

class TextureWindow : public Processor<RealVector, RealVector>
{
public:
    TextureWindow(unsigned win_len, unsigned win_step, unsigned in_dim);
    bool bang(const RealVector & input);

    const unsigned win_length;
    const unsigned win_step;

    const unsigned in_dimension;
    const unsigned dimension;

private:
    boost::circular_buffer<RealVector> buffer_;
    numeric_vec_t sum_;
    numeric_vec_t square_sum_;
};

#endif // TEXTURE_WINDOW_HPP
