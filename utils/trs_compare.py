#!/usr/bin/env python

from optparse import OptionParser
from transcription import Transcription, Comparator

def parseList(option, opt_string, value, parser):
    setattr(parser.values, option.dest, value.split())

if __name__ == '__main__':
    parser = OptionParser()
    
    parser.add_option('-i', '--input', 
                      type     = 'string',
                      dest     = 'testTranscriptions',
                      action   = 'callback',
                      callback = parseList,
                      help     = '.trs file(s) to be tested')
                      
    parser.add_option('-t', '--truth', 
                      type = 'string',
                      dest = 'truthTranscription',
                      help = 'ground truth .trs file')
                      
    parser.add_option('-o', '--output',
                      type = 'string',
                      dest = 'reportFile',
                      help = 'write a report at this file')
                      
    (options, args) = parser.parse_args()

    trs1 = Transcription(options.truthTranscription)
    trc = Comparator()
    for trs in options.testTranscriptions:
        trs2 = Transcription(trs)
        trc.bang(trs1, trs2)