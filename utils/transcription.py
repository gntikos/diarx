import xml.etree.ElementTree as ET
import numpy as np
from bartok import Bartok

class Turn:
    '''
    A simple class to hold a turn's properties together.
    Could just use a dictionary but this more clear.
    '''
    def __init__(self, speakerList, start, end):
        self.speakers = speakerList
        self.start    = start
        self.end      = end
    
    def __repr__(self):
        return '[Turn]: ' + str(self.start) + ' - ' + str(self.end) + \
            ' by ' + str(self.speakers)
        

class Transcription:
    def __init__(self, filename):
        tree = ET.ElementTree(file = filename)
        self.turns = [Turn(t.attrib['speaker'].split() or [''],
                           float(t.attrib['startTime']), 
                           float(t.attrib['endTime']))
                      for t in tree.find('Episode/Section').findall('Turn')]

        self.turns.sort(key = lambda turn: turn.start)
        
        self.speakerDict = {s.attrib['id']: s.attrib['name'] 
            for s in tree.find('Speakers').findall('Speaker')}
        self.speakerDict[''] = 'NO_SPEAKER' # explicitly add NO_SPEAKER
            
    def fillBlanks(self):
        '''
        Inserts empty turns in the non-transcribed time regions.
        '''
        for i in range(1, len(self.turns)):
            if self.turns[i].start - self.turns[i-1].end > 0.0:
                patch = Turn([''], self.turns[i-1].end, self.turns[i].start)
                self.turns.insert(i, patch)
                                             
                              
    def insertBreak(self, bTime):
        '''
        Adds a breakpoint to transcription.
        '''
        if bTime in [t.start for t in self.turns]:
            return
        if bTime in [t.end for t in self.turns]:
            return
        
        if bTime < self.turns[0].start:
            self.turns.insert(0, Turn([''], bTime, self.turns[0].start))
        elif bTime > self.turns[-1].end:
            self.turns.append(Turn([''], self.turns[-1].end, bTime))
        else:
            i = [k for k in range(len(self.turns)) 
                 if self.turns[k].end > bTime][0]
            t = Turn(self.turns[i].speakers, self.turns[i].start, bTime)
            
            self.turns.insert(i, t)
            self.turns[i+1].start = bTime
             

def align(trans1, trans2):
    '''
    Aligns two Transcriptions so that they both have the same breakpoints
    without altering their content.
    '''
    trans1.fillBlanks()
    trans2.fillBlanks()
    
    for t in trans1.turns:
        trans2.insertBreak(t.start)
        trans2.insertBreak(t.end)
        
    for t in trans2.turns:
        trans1.insertBreak(t.start)
        trans1.insertBreak(t.end)

class Comparator:
    '''
    Compares a "test" transcription to a "ground truth" one.
    
    Attributes:
    
    '''
    def __init__(self):
        self.truth = None
        self.test  = None
        self.truthDict = dict()
        self.testDict  = dict()
        self.hitMap = np.array([], dtype='double')
        self.assignment = Bartok()
        
    def bang(self, truth, test):
        self.truth = truth
        self.test  = test
        
        align(self.truth, self.test)
        self.getSpeakers()
        self.compare()
        self.evaluate()
        
    def getSpeakers(self):
        '''
        Create dictionaries with (key=speaker_id, val=row/column identifier),
        for speakers of both transcriptions. A row represents a speaker of the
        ground truth transcription and a column a speaker of the test
        transcription. 
        Assign the largest value (i.e. the last row/column) to the explicitly  
        created 'NO_SPEAKER' (key = ['']). This way, when removed from the 
        matrix, the other indices will not change.
        '''
        self.truthDict = dict(reversed(x) 
                              for x in enumerate(self.truth.speakerDict))
        lastRow = max(self.truthDict, key=self.truthDict.get)
        self.truthDict[lastRow], self.truthDict[''] = \
            self.truthDict[''], self.truthDict[lastRow]
            
        self.testDict = dict(reversed(x) 
                             for x in enumerate(self.test.speakerDict))
        lastCol = max(self.testDict, key=self.testDict.get)
        self.testDict[lastCol], self.testDict[''] = \
            self.testDict[''], self.testDict[lastCol]
        
        nRows = len(self.truthDict)
        nCols = len(self.testDict)
        self.hitMat = np.zeros((nRows, nCols), dtype='double')

    
    def compare(self):
        for n in range(len(self.test.turns)):
            duration = self.test.turns[n].end - self.test.turns[n].start
            for i in self.truth.turns[n].speakers:
                for j in self.test.turns[n].speakers:
                    self.hitMat[self.truthDict[i], self.testDict[j]] += duration
    
    def evaluate(self):
        self.assignment.bang(-self.hitMat[:-1:, :-1:])

        testSpeakers  = sorted(self.testDict, key=self.testDict.get)
        truthSpeakers = sorted(self.truthDict, key=self.truthDict.get)
        testSpeakers[-1] = truthSpeakers[-1] = '(NO SPEAKER)'
        
        print('\nConfusion Matrix')
        print(self.hitMat)
        rowDesc = 'Rows (speakers in ground truth transcription)\n'
        for i in range(len(truthSpeakers)):
            rowDesc += str(i+1) + ': ' + truthSpeakers[i] + '   '
        colDesc = 'Columns (speakers in testing transcription)\n'
        for j in range(len(testSpeakers)):
            colDesc += str(j+1) + ': ' + testSpeakers[j] + '   '

        print(rowDesc)
        print(colDesc)
        
        print('\n[Assignments]:')
        
        for x in self.assignment.solution:
            print(testSpeakers[x[1]], ' --- ', truthSpeakers[x[0]])
            
        print('\n[Recall]')
        template = '{0}: {1:.3f} sec. ({2:>.3f} sec. detected) RCL={3:.2%}' 
        
        for x in self.assignment.solution:
            speakerId = truthSpeakers[x[0]]
            total     = sum(self.hitMat[x[0],:])
            correct   = self.hitMat[x[0], x[1]]
            print(template.format(speakerId, total, correct, correct/total))
        
        print('\n[Precision]')
        template = '{0}: {1:.3f} sec. ({2:>.3f} sec. correct) PRC={3:.2%}'
        for x in self.assignment.solution:
            speakerId = testSpeakers[x[1]]
            total     = sum(self.hitMat[:,x[1]])
            correct   = self.hitMat[x[0], x[1]]
            print(template.format(speakerId, total, correct, correct/total))
        