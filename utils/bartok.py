import numpy as np

class Bartok:
    '''
    Béla Bartók (1881-1945), one of the most important Hungarian composers.
    
    Also, solves the assignment problem:
    "Given the n-by-n matrix [a_{ij}] of real numbers, find 
    a permutation p (p_i; i=1,...,n) of the integers 1, 2, ..., n 
    that minimizes \sum_{i=1}^{n} a_{i p_i}"
    
    The algorithm implemented is an extension of the popular Kuhn-Munkres
    algorithm, acting on rectangular (i.e. not only square) matrices. 
    It is described in
    "An extension of the Munkres algorithm for the assignment problem to 
    rectangular matrices", by Franqois Bourgeois and Jean-Claude Lassalle (1971)
    and a nice presentation can be found at
    http://csclab.murraystate.edu/bob.pilgrim/445/munkres.html
    '''    
    
    def __init__(self):
        self.mat  = np.array([], dtype='double')
        '''the cost matrix.'''
        
        self.mask = np.array([], dtype='unicode')
        '''A "mask" matrix of the same dimensions as self.mat, 
        keeping track of the starred (*) and primed (^) entries.'''
            
        self.isCoveredRow = list()
        '''List keeping track of the rows currently covered.'''
        
        self.isCoveredCol = list()
        '''List keeping track of the columns currently covered.'''
        
        self.nRows = int()
        '''Number of rows of the cost matrix.'''
        
        self.nCols = int()
        '''Number of columns of the cost matrix.'''
        
        self.solution = list()
        '''List of tuples containing the position of the elements
        that form the solution.'''
        
    
    def bang(self, matrix):
        self.mat = np.copy(matrix)
        self.initialize()
        self.starZeros()
        while not self.isOptimal():
            s = self.seedSolution()
            while not s:
                self.editByMin()
                s = self.seedSolution()
            self.proposeSolution(s)
    
    def initialize(self):
        '''
        Initializes class attributes and performs preliminary
        operations on the cost matrix.
        '''
        self.nRows, self.nCols = self.mat.shape
            
        self.isCoveredRow = self.nRows*[False]
        self.isCoveredCol = self.nCols*[False]
        
        self.mask = np.resize(self.mask, self.mat.shape)
        self.mask.fill('')
        
        if self.nCols >= self.nRows:
            for row in self.mat:
                row -= min(row)
        if self.nRows >= self.nCols:
            for col in self.mat.T:
                col -= min(col)

    
    def starZeros(self):
        ''' 
        Finds a zero (Z) in the matrix. If there is no starred zero
        in its row or column, (Z) gets starred. (repeated for all zeros)
        '''
        zeros = np.where(self.mat == 0)
        for i,j in zip(zeros[0], zeros[1]):
            if '*' not in self.mask[i,:] and '*' not in self.mask[:,j]:
                self.mask[i,j] = '*'
        
    def print(self):
        print(np.c_[self.mat, self.mask])
        print('R:', self.isCoveredRow, 'C:', self.isCoveredCol)
        
    def isOptimal(self):
        '''
        Checks the optimality of current configuration.
        
        Covers each column containing a starred zero.
        Returns True if number of covered columns is equal to the number
        of matrix's columns (the starred zeros describe a complete set 
        of unique assignments).
        '''
        for col in np.where(self.mask == '*')[1]:
            self.isCoveredCol[col] = True

        if self.isCoveredCol.count(True) == min(self.nRows, self.nCols):
            stars = np.where(self.mask == '*')
            self.solution = [s for s in zip(stars[0], stars[1])]
            return True
        else:
            return False
        
    def seedSolution(self):
        '''
        Covers all zeros of the matrix.
        Finds an uncovered zero (Z) and primes it.
        a. There is no starred zero in (Z)'s row 
        b. There is a starred zero (Z*) in (Z)'s row. This row is covered 
        and (Z*)'s column is uncovered. (repeated until all zeros are covered)
        '''
        zs_ = np.where(self.mat == 0)
        zeros = [(i,j) for (i,j) in zip(zs_[0], zs_[1])]
        allZerosCovered = self.isSetCovered(zeros)
        
        while not allZerosCovered:            
            for (i,j) in zeros:
                if not self.isCoveredRow[i] and not self.isCoveredCol[j]:
                    self.mask[i,j] = '^'
                    if '*' not in self.mask[i,:]:
                        return (i,j)
                    else:
                        starCol = np.where(self.mask[i,:]=='*')[0][0]
                        self.isCoveredRow[i] = True
                        self.isCoveredCol[starCol] = False
                        allZerosCovered = self.isSetCovered(zeros)
                        
        return False
                    
    def proposeSolution(self, initPos):
        '''
        Constructs a series of alternating primed and starred zeros.
        
        Starting from uncovered primed zero at initPos, the next element is
        the starred zero (Z*) in the same column (if any), followed by the 
        primed zero (Z^) at (Z*)'s row (always exists) and so on, until 
        terminating in a primed zero whose column doesn't contain any starred
        zeros.
        
        After that:
        a) every starred zero of the series is unstarred,
        b) every primed zero of the series is starred,
        c) all primes are erased and
        d) all lines of the matrix are uncovered
        
        Keyword arguments:
        initPos -- position of the first (primed) zero of the sequence
        '''
        seq = [initPos]
        seqTerminated = False
        
        while not seqTerminated:
            primeCol = seq[-1][1]
            starRows = np.where(self.mask[:,primeCol]=='*')[0]
            
            if len(starRows) == 0:
                seqTerminated = True
            else:
                starRow = starRows[0]
                seq.append((starRow, primeCol))
                seq.append((starRow, np.where(self.mask[starRow,:]=='^')[0][0]))
                
        for i,j in seq:
            if self.mask[i,j] == '*':
                self.mask[i,j] = ''
            elif self.mask[i,j] == '^':
                self.mask[i,j] = '*'
        
        for (i,j), value in np.ndenumerate(self.mask):
            if value == '^':
                self.mask[i,j] = ''
        
        self.isCoveredRow = [False for x in self.isCoveredRow]
        self.isCoveredCol = [False for x in self.isCoveredCol]
        
    
    def editByMin(self):
        '''
        Adds minimum uncovered value to elements of covered rows
        and substracts it from elements of uncovered columns.
        '''
        minUncovered = min([val for (i,j), val
            in np.ndenumerate(self.mat)
            if not self.isCoveredRow[i] and not self.isCoveredCol[j]])
            
        for i in [r for r in range(self.nRows) if self.isCoveredRow[r]]:
            self.mat[i,:] += minUncovered
        for j in [c for c in range(self.nCols) if not self.isCoveredCol[c]]:
            self.mat[:,j] -= minUncovered
            
    def isSetCovered(self, elems):
        '''
        Checks if a set of matrix's elements is covered
        '''
        return all([self.isCoveredRow[i] or self.isCoveredCol[j]
                    for (i,j) in elems])