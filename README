          _ _                 
         | (_)                
       __| |_  __ _ _ ____  __
      / _` | |/ _` | '__\ \/ /
     | (_| | | (_| | |   >  < 
the   \__,_|_|\__,_|_|  /_/\_\ project:
Speaker Diarization using FLsD method.

Reference: "Fisher Linear semi-Discriminant Analysis for Speaker Diarization",
            by Theodoros Giannakopoulos and Sergios Petridis (2012)
            
== [ build instructions ] =====================================================

To build everything:
    
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

project dependencies:

    CMake  (www.cmake.org)
    for building the project in a humane way.
    
    Boost  (www.boost.org)
    because everybody loves Boost.
    
    FFTW3  (www.fftw.org)
    for super-fast Fourier Transforms
    
    libsndfile (www.mega-nerd.com/libsndfile)
    for audio files manipulation.
    
    Eigen (http://eigen.tuxfamily.org)
    for fast and easy linear algebra operations
    

== [settings.json instructions] ===============================================

All settings are provided through the settings.json file
(or some other file of the same content).

+ The durations are in msec.

+ window_type
    0 -> Rectangle
    1 -> Hamming
    2 -> Hanning
    3 -> Bartlett
    4 -> Blackman
    5 -> Gaussian
    
+ window_param
    -1 -> default parameter
    
+ fft_size
    -1 -> 2^nextpow2(input size)
     0 -> same as input size
   
+ flsd.nominator, flsd.denominator
    1 -> Mixed-Class Scatter Matrix
    2 -> Within-Class Scatter Matrix
    3 -> Between-Class Scatter Matrix
    
+ flsd.criterion
    1 -> Trace of Ratio
    2 -> Ratio of Trace
    
+ threads_as_data
    (boolean) should speaker threads be used as data
              or use mid-term features instead?